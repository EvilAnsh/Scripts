#!/usr/bin/env bash

# Define Variables
DEVICE="RMX2001"
DT="https://github.com/EvilAnsh/twrp_device_realme_RMX2151"
OEM="realme"
SHRP_BRANCH="12.1"
TARGET=(
	recoveryimage
)
sudo apt update
sudo apt -y upgrade
sudo apt -y install gperf gcc-multilib gcc-10-multilib g++-multilib g++-10-multilib libc6-dev lib32ncurses5-dev x11proto-core-dev libx11-dev tree lib32z-dev libgl1-mesa-dev libxml2-utils xsltproc bc ccache lib32readline-dev lib32z1-dev liblz4-tool libncurses5-dev libsdl1.2-dev libxml2 lzop pngcrush schedtool squashfs-tools imagemagick libbz2-dev lzma ncftp qemu-user-static libstdc++-10-dev python
mkdir ~/bin
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo   
sudo ln -sf ~/bin/repo /usr/bin/repo
git config --global user.name "Ansh" && git config --global user.email "singhansh64321@gmail.com"
repo init --depth=1 -u https://github.com/SHRP/manifest.git -b shrp-${SHRP_BRANCH}
repo sync -j8 --force-sync --no-clone-bundle --no-tags
repo sync --force-sync

git clone ${DT} device/${OEM}/${DEVICE} -b SHRP-R6-${SHRP_BRANCH}

. build/envsetup.sh
export ALLOW_MISSING_DEPENDENCIES=true
export TW_PREPARE_DATA_MEDIA_EARLY=true
lunch twrp_${DEVICE}-eng
mka clean
mka -j$(nproc) ${TARGET}

cd ${OUT}
curl --upload-file *.zip https://free.keep.sh
curl --upload-file *.zip https://free.keep.sh
curl --upload-file *.img https://free.keep.sh
curl --upload-file *.img https://free.keep.sh
